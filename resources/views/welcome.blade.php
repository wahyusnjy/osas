<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            *{
                box-sizing: border-box;
            }
            
            html, body {
                background-color: #0d6efd;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .card-body {
              position: relative;
              display: flex;
              flex-direction: column;
              min-width: 0;
              word-wrap: break-word;
              background-color: #fff;
              background-clip: border-box;
              border: 1px solid rgba(0,0,0,.125);
              width: 100%;
              padding-top: 20px;
              border-radius: 15px;
            }
            .container{
                padding-top: 40px;
                padding-bottom: 40px;
            }

            .full-height {
                height: 42vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                padding-top: 25px;
                font-size: 40px;
                font-family: "poppins" sans-serif;
                font-weight: bold;
                color: #000;
            }
            .links {
                padding-top: 10px;
            }
            .links > a {
                color: #416BBC;
                padding: 0 25px;
                font-size: 30;
                font-weight: bold;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .desc {
                color: #7D7D7D;
                font-family: "poppins" sans-serif;
                font-weight: bold;
                font-size: 18px;
            }
            .footer {
                text-align: center;
                bottom: 0;
                position: absolute;

            }
            .footer .hak {
                color: #7b7b7b;
                font-family:  "poppins" sans-serif;
                font-size: 20px;
                font-weight: 800;
            }
            .footer .powered {
                color: #4494AD;
                font-family:  "poppins" sans-serif;
                font-size: 20px;
                font-weight: 700;
            }
            .login  {
                align-items: center;
                justify-content: center;
                display: flex; 
                padding-top: 200px; 
                 
            }
            
            
            .card-user {
                width: 319px;
                height: 157px;
                display: flex;
                align-items: center;
                background-clip: border-box;
                border: 1px solid rgba(0,0,0,.125);
                padding-left: 40px;
                border-radius: 10px;
                margin-left: 20px;
            }

            .card-user > img{
                width: 50px;
                border-radius: 50%;
                background-color: #0ADAF6; 
            }

            .card-user > .text-box{
                width: 100%;
                height: 100%;
                padding-left: 30px;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: flex-start;
                
                /* OKE UDAH NGAB */
                
            }
            
            .card-user > .text-box > a  {
                text-decoration: none;
                text-transform: uppercase;
                }
            .card-user > .text-box > p {
                margin-bottom: 8px;
                font-family: "poppins" sans-serif;
                font-weight: 700;
            }
            
            .card-user > .text-box > h4 {
                font-family: "corbel";
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="card-body">    
                
            <div class="content">
            <div class="links">
                    <a href="">Online School Attendance System</a>
                </div>
                <div class="title">
                    Selamat Datang
                </div>  
                <div class="desc">
                    Pilih Untuk Absen
                </div>
            </div>

            <div class="login">  
                @if (Route::has('login'))
                    <a href="{{ url ('/login')}}">
                    
                       <div class="row">
                        <div class="card-user">
                            <img class="card-img-left" src="asset\img\person2.png">
                            <div class="text-box">
                                <p class="card-text" >Saya Sebagai</p>
                                <h4 class="card-title">Siswa</h4>                              
                            </div>                  
                        </div>   
                           </a>
                           <a href="{{ url ('/login')}}">
                        <div class="card-user">
                            <img class="card-img-left" src="asset\img\person3.png">
                            <div class="text-box">
                                <p class="card-text ">Saya Sebagai</p>
                                <h4 class="card-title">Guru</h4>
                            </div>                       
                        </div>
                           </a>
                           @endif
                   </div>
            </div>
           
          <div class="flex-center position-ref full-height">
            <div class="footer">
                <div class="hak">
                ©Rexensoft 2021. Hak Cipta Dilindung Undang-Undang
                </div>
                <div class="powered">
                Powered by RexenSoft.
                </div>
            </div>
          </div>
        </div>
        </div>
        </div>
    </body>
</html>
